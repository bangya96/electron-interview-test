import { Injectable } from '@angular/core';
import PouchDB from 'pouchdb';
@Injectable({
  providedIn: 'root',
})
export class TokenService {
  pouchdb: any;
  private issuer = {
    login: 'http://test-demo.aemenersol.com/api/Account/Login',
  };
  constructor() {}
  handleData(token: any) {
    localStorage.setItem('auth_token', token);
  }
  getToken() {
    return localStorage.getItem('auth_token');
  }
  // Verify the token
  isValidToken() {
    const token = this.getToken();
    if (token) {
      return true;
    } else {
      return false;
    }
  }
  // Verify the token for local login
  localLogin() {
    const token = this.getToken();
    if (token === 'localhost') {
      return true;
    } else {
      return false;
    }
  }
  // User state based on valid token
  isLoggedIn() {
    return this.isValidToken();
  }
  // Remove token
  removeToken() {
    localStorage.removeItem('auth_token');
  }

  startDB(){
    this.pouchdb = new PouchDB("pouchdb");
    this.pouchdb.put({
      _id: 'user',
      username: 'local@gmail.com',
      password: 'localhost'
    }).then(function (response) {
      // handle response
    }).catch(function (err) {
      console.log(err);
    });
  }

  async signInLocalDB(loginForm){
    var item;
    await this.pouchdb.get('user').then(function (doc) {
      item = doc;
    }).catch(function (err) {
      console.log(err);
    });
    console.log(item)
    console.log(loginForm)
    if (item.username === loginForm.username && item.password === loginForm.password){
      return true;
    } else {
      return false;
    }
  }
}
